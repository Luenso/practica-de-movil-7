package solorzanomedinaluis.facci.practica7_4b;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ActivityPasarParametros extends AppCompatActivity {
    Button pasarParametros;
    EditText etNombre, etApellidos, etCelular, etEmail;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pasar_parametros);
        etNombre = findViewById(R.id.etNombres);
        etApellidos = findViewById(R.id.etApellidos);
        etCelular = findViewById(R.id.etCelular);
        etEmail = findViewById(R.id.etEmail);
        pasarParametros = findViewById(R.id.buttonEnviarParametros);

        pasarParametros.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityPasarParametros.this, ActivityRecibirParametros.class);
                Bundle bundle = new Bundle();
                bundle.putString("nombres", etNombre.getText().toString());
                bundle.putString("apellidos", etApellidos.getText().toString());
                bundle.putString("celular", etCelular.getText().toString());
                bundle.putString("email", etEmail.getText().toString());
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }
}
