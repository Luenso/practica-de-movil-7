package solorzanomedinaluis.facci.practica7_4b;

import android.content.Context;
import android.content.Intent;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Button buttonLogin, buttonBuscar, buttonGuardar, buttonParametro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        buttonLogin = findViewById(R.id.Buttonlogin);
        buttonBuscar = findViewById(R.id.Buttonbuscar);
        buttonGuardar = findViewById(R.id.Buttonguardar);
        buttonParametro = findViewById(R.id.buttonPasarParametros);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        final Vibrator vibrator = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);

        switch (item.getItemId()){
            case R.id.opcionLogin:
                //intent = new Intent(MainActivity.this, ActivityLogin.class);
                //startActivity(intent);
                break;
            case R.id.opcionRegistrar:
                //intent = new Intent(MainActivity.this, ActivityRegistrar.class);
                //startActivity(intent);
                break;
            case R.id.opcionVibracion:
                vibrator.vibrate(600);
                break;

        }
        return true;
    }
}
