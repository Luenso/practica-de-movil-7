package solorzanomedinaluis.facci.practica7_4b;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class ActivityRecibirParametros extends AppCompatActivity {
    TextView txtnombres, txtapellidos, txtcelular, txtemail;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibir_parametros);
        txtnombres = findViewById(R.id.txtNombre);
        txtapellidos = findViewById(R.id.txtApellidos);
        txtcelular = findViewById(R.id.txtCelular);
        txtemail = findViewById(R.id.txtEmail);
        Bundle bundle = this.getIntent().getExtras();
        txtnombres.setText(bundle.getString("nombres"));
        txtapellidos.setText(bundle.getString("apellidos"));
        txtcelular.setText(bundle.getString("celular"));
        txtemail.setText(bundle.getString("email"));

    }
}
